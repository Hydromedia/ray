#include "sphere.h"

Sphere::Sphere(NormalRenderer *normalRenderer, GLuint shader)
    : Shape(normalRenderer, shader)
{
    m_shapeType = 2;
}

Sphere::~Sphere()
{

}

void Sphere::updateBuffer(int p1, int p2, int p3){
    if (p2 < 3) {
        p2 = 3;
    }
    if (p1 < 2) {
        p1 = 2;
    }
    m_bufferSize = 2*p1*p2*3*8;
    m_bufferData = new GLfloat[m_bufferSize];
    m_vertices = m_bufferSize/6;

    float r = .5;
    int buffer_counter = 0;
    float phi_iter_amount = (M_PI)/((float) p1);
    float theta_iter_amount = (2.f*M_PI)/((float) p2);
    float theta = 0;
    float phi = 0;

    //Adds the sides
    for (int p2_iter = 0; p2_iter < p2; p2_iter++) {
        phi = 0;
        for (int p1_iter = 0; p1_iter < p1; p1_iter++) {

            float x1a = std::sin(phi)*std::cos(theta)*r;
            float x1b = std::sin(phi)*std::cos(theta+theta_iter_amount)*r;
            float x2a = std::sin(phi+phi_iter_amount)*std::cos(theta)*r;
            float x2b = std::sin(phi+phi_iter_amount)*std::cos(theta+theta_iter_amount)*r;

            float y1a = r*std::cos(phi);
            float y1b = r*std::cos(phi);
            float y2a = r*std::cos(phi+phi_iter_amount);
            float y2b = r*std::cos(phi+phi_iter_amount);

            float z1a = std::sin(phi)*std::sin(theta)*r;
            float z1b = std::sin(phi)*std::sin(theta+theta_iter_amount)*r;
            float z2a = std::sin(phi+phi_iter_amount)*std::sin(theta)*r;
            float z2b = std::sin(phi+phi_iter_amount)*std::sin(theta+theta_iter_amount)*r;

            buffer_counter = addTriangle(m_bufferData, buffer_counter,
                                         x1a, y1a, z1a,
                                         x1b, y1b, z1b,
                                         x2a, y2a, z2a,
                             glm::normalize(glm::vec3(x1a/r, y1a/r, z1a/r)),
                             glm::normalize(glm::vec3(x1b/r, y1b/r, z1b/r)),
                             glm::normalize(glm::vec3(x2a/r, y2a/r, z2a/r)),
                             true, 0, 0);

            buffer_counter = addTriangle(m_bufferData, buffer_counter,
                                        x1b, y1b, z1b,
                                        x2a, y2a, z2a,
                                        x2b, y2b, z2b,
                             glm::normalize(glm::vec3(x1b/r, y1b/r, z1b/r)),
                             glm::normalize(glm::vec3(x2a/r, y2a/r, z2a/r)),
                             glm::normalize(glm::vec3(x2b/r, y2b/r, z2b/r)),
                             false, 0, 0);
            phi += phi_iter_amount;

        }
        theta += theta_iter_amount;
    }

    handleBuffers();

    // Don't forget to clean up any resources you created
    if (m_bufferData != 0) {
        delete [] m_bufferData;
    }
}

