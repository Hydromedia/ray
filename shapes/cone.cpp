#include "cone.h"

Cone::Cone(NormalRenderer *normalRenderer, GLuint shader)
    : Shape(normalRenderer, shader)
{
    m_shapeType = 1;
}

Cone::~Cone()
{

}

void Cone::updateBuffer(int p1, int p2, int p3){
    if (p2 < 3) {
        p2 = 3;
    }
    m_bufferSize = ((p1*2-1)*(p2))*3*8 + 2*p1*p2*3*8;
    m_bufferData = new GLfloat[m_bufferSize];
    m_vertices = m_bufferSize/6;
    float r = .5;
    int buffer_counter = 0;
    float iter_amount = (2*M_PI)/((float) p2);
    float y = -.5f;
    float x = 0;
    float z = 0;

    //Adds the bottom
    for (int p2_iter = 0; p2_iter < p2; p2_iter++) {
        int p1_iter_max = p1;
        for (float p1_iter = 0; p1_iter < p1_iter_max; p1_iter += 1.f) {
            bool up = y > 0 ? true : false;
            float iter_prop = p1_iter / ((float) p1_iter_max);
            float iter_prop2 = (1.f+p1_iter) / ((float) p1_iter_max);
            if (p1_iter != 0) {
                buffer_counter = addTriangle(m_bufferData, buffer_counter,
                                                 (iter_prop)*std::cos(x)*r, y, iter_prop*std::sin(z)*r,
                                                 (iter_prop)*std::cos(x+iter_amount)*r, y, (iter_prop)*std::sin(z+iter_amount)*r,
                                                 (iter_prop2)*std::cos(x)*r, y, (iter_prop2)*std::sin(z)*r,
                                                 glm::vec3(0,y*2.f,0), glm::vec3(0,y*2.f,0), glm::vec3(0,y*2.f,0), up,
                                                 x,z);

                buffer_counter = addTriangle(m_bufferData, buffer_counter,
                                                 (iter_prop)*std::cos(x+iter_amount)*r, y, (iter_prop)*std::sin(z+iter_amount)*r,
                                                 (iter_prop2)*std::cos(x)*r, y, (iter_prop2)*std::sin(z)*r,
                                                 (iter_prop2)*std::cos(x+iter_amount)*r, y, (iter_prop2)*std::sin(z+iter_amount)*r,
                                                 glm::vec3(0,y*2.f,0), glm::vec3(0,y*2.f,0), glm::vec3(0,y*2.f,0), !up,
                                                 x,z);
            } else {
                buffer_counter = addTriangle(m_bufferData, buffer_counter,
                                                 (iter_prop)*std::cos(x)*r, y, (iter_prop)*std::sin(z)*r,
                                                 (iter_prop2)*std::cos(x)*r, y, (iter_prop2)*std::sin(z)*r,
                                                 (iter_prop2)*std::cos(x+iter_amount)*r, y, (iter_prop2)*std::sin(z+iter_amount)*r,
                                                 glm::vec3(0,y*2.f,0), glm::vec3(0,y*2.f,0), glm::vec3(0,y*2.f,0), !up,
                                                 x,z);
            }
        }
        z+=iter_amount;
        x+=iter_amount;
    }

    //Adds the sides
    iter_amount = (2*M_PI)/((float) p2);
    x = 0;
    z = 0;
    //(2/(sqrt(5)))*
    for (int p2_iter = 0; p2_iter < p2; p2_iter++) {
        int p1_iter_max = p1;
        for (float p1_iter = 0; p1_iter < p1_iter_max; p1_iter += 1.f) {
            float iter_prop = p1_iter / ((float) p1_iter_max);
            float iter_prop2 = (1.f+p1_iter) / ((float) p1_iter_max);

            float x1a = iter_prop*std::cos(x)*r;
            float x1b = iter_prop*std::cos(x+iter_amount)*r;
            float x2a = iter_prop2*std::cos(x)*r;
            float x2b = iter_prop2*std::cos(x+iter_amount)*r;

            float z1a = iter_prop*std::sin(z)*r;
            float z1b = iter_prop*std::sin(z+iter_amount)*r;
            float z2a = iter_prop2*std::sin(z)*r;
            float z2b = iter_prop2*std::sin(z+iter_amount)*r;

            buffer_counter = addTriangle(m_bufferData, buffer_counter,
                                             x1a, .5f-iter_prop, z1a,
                                             x1b, .5f-iter_prop, z1b,
                                             x2a, .5f-iter_prop2, z2a,
                                             glm::normalize(glm::vec3(2*x1a/(sqrt(5)*r*iter_prop),
                                                                      1.f/sqrt(5.f),
                                                                      2*z1a/(sqrt(5)*r*iter_prop))),
                                             glm::normalize(glm::vec3(2*x1b/(sqrt(5)*r*iter_prop),
                                                                      1.f/sqrt(5.f),
                                                                      2*z1b/(sqrt(5)*r*iter_prop))),
                                             glm::normalize(glm::vec3(2*x2a/(sqrt(5)*r*iter_prop2),
                                                                      1.f/sqrt(5.f),
                                                                      2*z2a/(sqrt(5)*r*iter_prop2))),
                                             true,
                                             x, z);

            buffer_counter = addTriangle(m_bufferData, buffer_counter,
                                             x1b, .5f-iter_prop, z1b,
                                             x2a, .5f-iter_prop2, z2a,
                                             x2b, .5f-iter_prop2, z2b,
                                             glm::normalize(glm::vec3(2*x1b/(sqrt(5)*r*iter_prop),
                                                                      1.f/sqrt(5.f),
                                                                      2*z1b/(sqrt(5)*r*iter_prop))),
                                             glm::normalize(glm::vec3(2*x2a/(sqrt(5)*r*iter_prop2),
                                                                      1.f/sqrt(5.f),
                                                                      2*z2a/(sqrt(5)*r*iter_prop2))),
                                             glm::normalize(glm::vec3(2*x2b/(sqrt(5)*r*iter_prop2),
                                                                      1.f/sqrt(5.f),
                                                                      2*z2b/(sqrt(5)*r*iter_prop2))),
                                             false,
                                             x, z);
        }
        z+=iter_amount;
        x+=iter_amount;
    }



    handleBuffers();

    // Don't forget to clean up any resources you created
    if (m_bufferData != 0) {
        delete [] m_bufferData;
    }
}
