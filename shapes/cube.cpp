#include "cube.h"

Cube::Cube(NormalRenderer *normalRenderer, GLuint shader)
    : Shape(normalRenderer, shader)
{
    m_shapeType = 0;
}

Cube::~Cube()
{

}



void Cube::updateBuffer(int p1, int p2, int p3){

    //2*n^2 * 6 faces * 3 verts per triangle * 6 floats per vert
    m_bufferSize = (2*p1*p1)*6*3*8;
    m_bufferData = new GLfloat[m_bufferSize];
    m_vertices = m_bufferSize/6;

    float x_size = 1;
    float y_size = 1;
    float z_size = 1;

    int buffer_counter = 0;

    for(float x = -1; x <= 1; x+=2){
        float y = -.5;
        for(int y_iter = 0; y_iter < p1; y_iter++) {
            float z = -.5;
            for(int z_iter = 0; z_iter < p1; z_iter++) {
                if (x == -1) {
                    buffer_counter = addPlanePerpSquare(m_bufferData, buffer_counter, 0, y, z, 0, y+y_size/((float) p1), z+z_size/((float) p1), glm::vec3(x,0,0), false, 0, 0 );
                } else {
                    buffer_counter = addPlanePerpSquare(m_bufferData, buffer_counter, 0, y, z, 0, y+y_size/((float) p1), z+z_size/((float) p1), glm::vec3(x,0,0), true, 0, 0);
                }
                z += z_size/(float) p1;
            }
            y +=y_size/(float) p1;
        }
    }

    for(float y = -1; y <= 1; y+=2){
        float x = -.5;
        for(int x_iter = 0; x_iter < p1; x_iter++) {
            float z = -.5;
            for(int z_iter = 0; z_iter < p1; z_iter++) {
                if (y == -1) {
                    buffer_counter = addPlanePerpSquare(m_bufferData, buffer_counter, x, 0, z, x+x_size/((float) p1), 0, z+z_size/((float) p1), glm::vec3(0,y,0), true, 0, 0);
                } else {
                    buffer_counter = addPlanePerpSquare(m_bufferData, buffer_counter, x, 0, z, x+x_size/((float) p1), 0, z+z_size/((float) p1), glm::vec3(0,y,0), false, 0, 0);
                }
                z += z_size/(float) p1;
            }
            x += x_size/(float) p1;
        }
    }

    for(float z = -1; z <= 1; z+=2){
        float x = -.5;
        for(int x_iter = 0; x_iter < p1; x_iter++) {
            float y = -.5;
            for(int y_iter = 0; y_iter < p1; y_iter++) {
                if (z == -1) {
                    buffer_counter = addPlanePerpSquare(m_bufferData, buffer_counter, x, y, 0, x+x_size/((float) p1), y+y_size/((float) p1), 0, glm::vec3(0,0,z), false, 0, 0);
                } else {
                    buffer_counter = addPlanePerpSquare(m_bufferData, buffer_counter, x, y, 0, x+x_size/((float) p1), y+y_size/((float) p1), 0, glm::vec3(0,0,z), true, 0, 0);
                }
                y +=y_size/(float) p1;
            }
            x += x_size/(float) p1;
        }
    }

    handleBuffers();

    // Don't forget to clean up any resources you created
    if (m_bufferData != 0) {
        delete [] m_bufferData;
    }
}
