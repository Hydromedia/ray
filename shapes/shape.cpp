#include "shape.h"


Shape::Shape(NormalRenderer *normalRenderer, GLuint shader) :
    m_normalRenderer(normalRenderer), m_shader(shader)
{

}

Shape::~Shape()
{

}

void Shape::draw()
{
    glBindVertexArray(m_vaoID);
    glDrawArrays(GL_TRIANGLES, 0, m_vertices);
    glBindVertexArray(0);
}

int Shape::round(float f){
    if (f > 0) {
        return (int)(f+0.5);
    } else {
        return (int)(f-0.5);
    }
}

void Shape::handleBuffers() {
    // Initialize the vertex array object.
    glGenVertexArrays(1, &m_vaoID);
    glBindVertexArray(m_vaoID);

    // Initialize the vertex buffer object.
    GLuint vertexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);

    // Pass vertex data to OpenGL.
    glBufferData(GL_ARRAY_BUFFER, m_bufferSize * sizeof(GLfloat), m_bufferData, GL_STATIC_DRAW);
    glEnableVertexAttribArray(glGetAttribLocation(m_shader, "position"));
    glEnableVertexAttribArray(glGetAttribLocation(m_shader, "normal"));
    glVertexAttribPointer(
       glGetAttribLocation(m_shader, "position"),
       3,                   // Num coordinates per position
       GL_FLOAT,            // Type
       GL_FALSE,            // Normalized
       sizeof(GLfloat) * 8, // Stride
       (void*) 0            // Array buffer offset
    );
    glVertexAttribPointer(
       glGetAttribLocation(m_shader, "normal"),
       3,           // Num coordinates per normal
       GL_FLOAT,    // Type

       GL_TRUE,     // Normalized
       sizeof(GLfloat) * 6,           // Stride
       (void*) (sizeof(GLfloat) * 3)    // Array buffer offset
    );

    glVertexAttribPointer(
       glGetAttribLocation(m_shader, "texCoord"),
       2,           // Num coordinates per textureCoordinate
       GL_FLOAT,    // Type
       GL_FALSE,     // Normalized
       sizeof(GLfloat) * 8,           // Stride
       (void*) (sizeof(GLfloat) * 6)    // Array buffer offset
    );

    // Initialize normals so they can be displayed with arrows. (This can be very helpful
    // for debugging!)
    // This object (m_normalRenderer) can be passed around to other classes,
    // make sure to include "OpenGLScene.h" in any class you want to use the NormalRenderer!
    // generateArrays will take care of any cleanup from the previous object state.
    m_normalRenderer->generateArrays(
                m_bufferData,             // Pointer to vertex data
                6 * sizeof(GLfloat),    // Stride (distance between consecutive vertices/normals in BYTES
                0,                      // Offset of first position in BYTES
                3 * sizeof(GLfloat),    // Offset of first normal in BYTES
                m_vertices);                     // Number of vertices

    // Unbind buffers.
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

int Shape::addTriangle(float *buffer, int counter, float x1, float y1, float z1,
                                                   float x2, float y2, float z2,
                                                   float x3, float y3, float z3,
                                                   glm::vec3 normal1, glm::vec3 normal2, glm::vec3 normal3, bool isUp, float u, float v)
{
    if (isUp) {
        counter = addVertex(buffer, counter, x1, y1, z1, normal1, u, v);
        counter = addVertex(buffer, counter, x2, y2, z2, normal2, u, v);
        counter = addVertex(buffer, counter, x3, y3, z3, normal3, u, v);
    } else {
        counter = addVertex(buffer, counter, x3, y3, z3, normal3, u, v);
        counter = addVertex(buffer, counter, x2, y2, z2, normal2, u, v);
        counter = addVertex(buffer, counter, x1, y1, z1, normal1, u, v);
    }
    return counter;
}

int Shape::addVertex(float* buffer, int counter, float x, float y, float z, glm::vec3 normal, float u, float v){
    buffer[counter] = x;
    buffer[counter+1] = y;
    buffer[counter+2] = z;

    normal = glm::normalize(normal);
    buffer[counter+3] = normal.x;
    buffer[counter+4] = normal.y;
    buffer[counter+5] = normal.z;

    buffer[counter+6] = u;
    buffer[counter+7] = v;
    return counter + 8;
}

int Shape::addPlanePerpSquare(float* buffer, int counter, float x1, float y1, float z1, float x2, float y2, float z2, glm::vec3 normal, bool isUp,
                              float u, float v) {
    if (normal.x != 0) {
        if (isUp) {
            counter = addVertex(buffer, counter, normal.x/2.f, y1, z1, normal, u, v);
            counter = addVertex(buffer, counter, normal.x/2.f, y2, z2, normal, u, v);
            counter = addVertex(buffer, counter, normal.x/2.f, y1, z2, normal, u, v);

            counter = addVertex(buffer, counter, normal.x/2.f, y1, z1, normal, u, v);
            counter = addVertex(buffer, counter, normal.x/2.f, y2, z1, normal, u, v);
            counter = addVertex(buffer, counter, normal.x/2.f, y2, z2, normal, u, v);
        } else {
            counter = addVertex(buffer, counter, normal.x/2.f, y1, z2, normal, u, v);
            counter = addVertex(buffer, counter, normal.x/2.f, y2, z2, normal, u, v);
            counter = addVertex(buffer, counter, normal.x/2.f, y1, z1, normal, u, v);

            counter = addVertex(buffer, counter, normal.x/2.f, y2, z2, normal, u, v);
            counter = addVertex(buffer, counter, normal.x/2.f, y2, z1, normal, u, v);
            counter = addVertex(buffer, counter, normal.x/2.f, y1, z1, normal, u, v);
        }
    } else if (normal.y != 0) {
        if (isUp) {
            counter = addVertex(buffer, counter, x1, normal.y/2.f, z1, normal, u, v);
            counter = addVertex(buffer, counter, x2, normal.y/2.f, z2, normal, u, v);
            counter = addVertex(buffer, counter, x1, normal.y/2.f, z2, normal, u, v);

            counter = addVertex(buffer, counter, x1, normal.y/2.f, z1, normal, u, v);
            counter = addVertex(buffer, counter, x2, normal.y/2.f, z1, normal, u, v);
            counter = addVertex(buffer, counter, x2, normal.y/2.f, z2, normal, u, v);
        } else {
            counter = addVertex(buffer, counter, x1, normal.y/2.f, z2, normal, u, v);
            counter = addVertex(buffer, counter, x2, normal.y/2.f, z2, normal, u, v);
            counter = addVertex(buffer, counter, x1, normal.y/2.f, z1, normal, u, v);

            counter = addVertex(buffer, counter, x2, normal.y/2.f, z2, normal, u, v);
            counter = addVertex(buffer, counter, x2, normal.y/2.f, z1, normal, u, v);
            counter = addVertex(buffer, counter, x1, normal.y/2.f, z1, normal, u, v);
        }
    } else if (normal.z != 0) {
        if (isUp) {
            counter = addVertex(buffer, counter, x1, y1, normal.z/2.f, normal, u, v);
            counter = addVertex(buffer, counter, x2, y2, normal.z/2.f, normal, u, v);
            counter = addVertex(buffer, counter, x1, y2, normal.z/2.f, normal, u, v);

            counter = addVertex(buffer, counter, x1, y1, normal.z/2.f, normal, u, v);
            counter = addVertex(buffer, counter, x2, y1, normal.z/2.f, normal, u, v);
            counter = addVertex(buffer, counter, x2, y2, normal.z/2.f, normal, u, v);
        } else {
            counter = addVertex(buffer, counter, x1, y2, normal.z/2.f, normal, u, v);
            counter = addVertex(buffer, counter, x2, y2, normal.z/2.f, normal, u, v);
            counter = addVertex(buffer, counter, x1, y1, normal.z/2.f, normal, u, v);

            counter = addVertex(buffer, counter, x2, y2, normal.z/2.f, normal, u, v);
            counter = addVertex(buffer, counter, x2, y1, normal.z/2.f, normal, u, v);
            counter = addVertex(buffer, counter, x1, y1, normal.z/2.f, normal, u, v);
        }
    }
    return counter;
}
