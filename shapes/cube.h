#ifndef CUBE_H
#define CUBE_H
#include "shape.h"

class Cube : public Shape
{
public:
    Cube(NormalRenderer *normalRenderer, GLuint shader);
    ~Cube();
    void updateBuffer(int p1, int p2, int p3);
};

#endif // CUBE_H
