/**
 * @file   Brush.cpp
 *
 * Implementation of common functionality of bitmap brushes.
 *
 * You should fill this file in while scompleting the Brush assignment.
 */

#include "Brush.h"
#include "Canvas2D.h"
#include "Settings.h"

unsigned char lerp(unsigned char a, unsigned char b, float percent)
{
    float fa = (float)a / 255.0f;
    float fb = (float)b / 255.0f;
    return (unsigned char)((fa + (fb - fa) * percent) * 255.0f + 0.5f);
}



Brush::Brush(BGRA color, int flow, int radius) {

    // @TODO: [BRUSH] Initialize any memory you are going to use here. Hint - you are going to
    //        need to store the mask in memory. This might (or might not) be a good place to
    //        allocate that memory.

    // Example code: (feel free to use)
    m_color = color;
    m_flow = flow;
    m_radius = radius;
    m_mask = new float[(2*m_radius+1)*(2*m_radius+1)];
}


Brush::~Brush()
{
    // @TODO: [BRUSH] Don't forget to delete any memory you allocate. Use delete[] to delete
    //        a whole array. Otherwise you'll just delete the first element!
    //
    //        i.e. delete[] m_mask;
    //

    delete[] m_mask;
}


void Brush::setGreen(int green)
{
    m_color.g = green;
}


void Brush::setRed(int red)
{
    m_color.r = red;
}


void Brush::setBlue(int blue)
{
    m_color.b = blue;
}


void Brush::setFlow(int flow)
{
    m_flow = flow;
    makeMask(m_radius*2 + 1, m_radius*2 + 1, m_radius, m_radius);
}


void Brush::setRadius(int radius)
{
    m_radius = radius;
    makeMask(m_radius*2 + 1, m_radius*2 + 1, m_radius, m_radius);
}


void Brush::paintOnce(int mouse_x, int mouse_y, Canvas2D* canvas)
{
    // @TODO: [BRUSH] You can do any painting on the canvas here. Or, you can
    //        override this method in a subclass and do the painting there.
    //
    // Example: You'll want to delete or comment out this code, which
    // sets all the pixels on the canvas to red.
    //

    BGRA* pix = canvas->data();
    int rowstart = max(0, mouse_y-m_radius);
    int rowend = min(canvas->height(), mouse_y+m_radius+1);
    int colstart = max(0, mouse_x-m_radius);
    int colend = min(canvas->width(), mouse_x+m_radius+1);

    int xMid = m_radius;
    int yMid = m_radius;
    if (0 > mouse_y-m_radius) {
        yMid = rowend - m_radius;
    }
    if (0 > mouse_x-m_radius) {
        xMid = colend - m_radius;
    }

    makeMask(rowend-rowstart, colend-colstart, xMid, yMid);

    float alpha = ((float) m_color.a) / ((float) 255);
    float red = ((float) m_color.r) / ((float) 255);
    float green = ((float) m_color.g) / ((float) 255);
    float blue = ((float) m_color.b) / ((float) 255);


    //std::cout << "Rowstart: " << rowstart << ", rowend: " << rowend << ", colstart: " << colstart << ", colend: " << colend << std::endl;

    //std::cout << "Red: " << ((int) m_color.r) << std::endl;
    //std::cout << "Red value at pixel_start: " << ((int) pix[pixel_start].r) << std::endl;
    //std::cout << pixel_start << std::endl;
    //std::cout << "Result: " << m_mask[0] * .5 * m_color.r + pix[pixel_start].r * (1 -  m_mask[0] * .5) << std::endl;
    int mask_index = 0;
    for (int rowcounter = rowstart; rowcounter < rowend; rowcounter++) {
        for (int colcounter = colstart; colcounter < colend; colcounter++) {
            int pixel_index = rowcounter*canvas->size().width() + colcounter;

            //std::cout << "Red value at pixel_start: " << ((int) pix[pixel_start].r) << std::endl;
            //std::cout << "Pixel Start: " << pixel_start << ", Pixel Index: " << pixel_index << std::endl;
            //std::cout << "Result: " << m_mask[0] * m_color.a * m_color.r + pix[pixel_start].r * (1 -  m_mask[0] * m_color.a) << std::endl;
            //std::cout << "Mask Index: " << mask_index << ", Pixel Index: " << pixel_index << ", Pixels: " << canvas->width()*canvas->height() << std::endl;

            float canvas_red = ((float) pix[pixel_index].r) / ((float) 255);
            float canvas_green = ((float) pix[pixel_index].g) / ((float) 255);
            float canvas_blue = ((float) pix[pixel_index].b) / ((float) 255);

            //std::cout << "Red1: " << canvas_red << std::endl;
            //std::cout << "Red2: " << red << std::endl;

            // F = M * alpha * B + C * (1 - M * alpha)
            //std::cout << mask_index << std::endl;
            pix[pixel_index].r = 255*(m_mask[mask_index] * alpha * red + canvas_red * (1 -  m_mask[mask_index] * alpha));
            pix[pixel_index].g = 255*(m_mask[mask_index] * alpha * green + canvas_green * (1 -  m_mask[mask_index] * alpha));
            pix[pixel_index].b = 255*(m_mask[mask_index] * alpha * blue + canvas_blue * (1 -  m_mask[mask_index] * alpha));
            pix[pixel_index].a = 255;
            mask_index++;
        }
    }

}


