/**
 * @file   ConstantBrush.cpp
 *
 * Implementation of a brush with a constant mask distribution.
 *
 * You should fill this file in while completing the Brush assignment.
 */

#include "ConstantBrush.h"
#include <cmath>

ConstantBrush::ConstantBrush(BGRA color, int flow, int radius)
    : Brush(color, flow, radius)
{
    // @TODO: [BRUSH] You'll probably want to set up the mask right away.
    makeMask(2*m_radius+1, 2*m_radius+1, m_radius, m_radius);
}


ConstantBrush::~ConstantBrush()
{
    // @TODO: [BRUSH] Delete any resources owned by this brush, so you don't leak memory.

}

void ConstantBrush::makeMask(int rows, int cols, int xMid, int yMid)
{
    // @TODO: [BRUSH] Set up the mask for your Constant brush here...

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++){
            if (sqrt((xMid - j)*(xMid - j) + (yMid - i)*(yMid - i)) <= m_radius) {
                m_mask[i*cols + j] = 1;
            } else {
                 m_mask[i*cols + j] = 0;
            }
        }
    }
}


