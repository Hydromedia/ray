/**
 * @file   QuadraticBrush.cpp
 *
 * Implementation of a brush with a quadratic mask distribution.
 *
 * You should fill this file in while completing the Brush assignment.
 */

#include "QuadraticBrush.h"

QuadraticBrush::QuadraticBrush(BGRA color, int flow, int radius)
    : Brush(color, flow, radius)
{
    // @TODO: [BRUSH] You'll probably want to set up the mask right away.
    //makeMask();
}

QuadraticBrush::~QuadraticBrush()
{
    // @TODO: [BRUSH] Delete any resources owned by this brush, so you don't leak memory.

}

void QuadraticBrush::makeMask(int rows, int cols, int xMid, int yMid)
{
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++){
            float distance = sqrt((xMid - j)*(xMid - j) + (yMid - i)*(yMid - i));
            if (distance <= m_radius) {
                m_mask[i*cols + j] = ((m_radius-distance) / ((float) m_radius)) / ((float) m_radius);
            } else {
                 m_mask[i*cols + j] = 0;
            }
        }
    }
}


