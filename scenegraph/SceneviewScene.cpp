#include "SceneviewScene.h"
#include "GL/glew.h"
#include <QGLWidget>
#include "Camera.h"
#include <iostream>

#include "shapes/cone.h"
#include "shapes/cube.h"
#include "shapes/sphere.h"
#include "shapes/cylinder.h"


SceneviewScene::SceneviewScene()
{
    // TODO: [SCENEVIEW] Set up anything you need for your Sceneview scene here...
}

SceneviewScene::~SceneviewScene()
{
    // TODO: [SCENEVIEW] Don't leak memory!
}


void SceneviewScene::init()
{
    OpenGLScene::init();
    m_cube.reset(new Cube(m_normalRenderer, m_shader));
    m_cone.reset(new Cone(m_normalRenderer, m_shader));
    m_cylinder.reset(new Cylinder(m_normalRenderer, m_shader));
    m_sphere.reset(new Sphere(m_normalRenderer, m_shader));
}

void SceneviewScene::setLights(const glm::mat4 viewMatrix)
{
    clearLights();
    for (int i = 0; i < m_sceneLights.length(); i++){
        setLight(m_sceneLights.at(i));
    }
}

void SceneviewScene::renderGeometry()
{
    if (!m_initialized){
        applyLOD(m_sceneGraph.length());
        m_initialized = true;
    }

    for (int i = 0; i < m_sceneGraph.length(); i++) {

        if (m_sceneGraph.at(i).primitive.material.textureMap->isUsed) {
            glBindTexture(GL_TEXTURE_2D, m_sceneGraph.at(i).primitive.material.textureMap->texid);
        }

        glUniformMatrix4fv(m_uniformLocs["m"], 1, GL_FALSE, glm::value_ptr(m_sceneGraph.at(i).transformation));
        applyMaterial(m_sceneGraph.at(i).primitive.material);

        if (m_sceneGraph.at(i).primitive.type == PRIMITIVE_CUBE) {
            m_cube->draw();
        } else if (m_sceneGraph.at(i).primitive.type == PRIMITIVE_CONE) {
            m_cone->draw();
        } else if (m_sceneGraph.at(i).primitive.type == PRIMITIVE_CYLINDER) {
            m_cylinder->draw();
        } else if (m_sceneGraph.at(i).primitive.type == PRIMITIVE_SPHERE) {
            m_sphere->draw();
        }

        if (m_sceneGraph.at(i).primitive.material.textureMap->isUsed) {
            glBindTexture(GL_TEXTURE_2D, 0);
        }
    }
}


void SceneviewScene::setSelection(int x, int y)
{
    // TODO: [MODELER LAB] Fill this in...
    //
    // Using m_selectionRecorder, set m_selectionIndex to the index in your
    // flattened parse tree of the object under the mouse pointer.  The
    // selection recorder will pick the object under the mouse pointer with
    // some help from you, all you have to do is:

    // 1) Set this to the number of objects you will be drawing.
    int numObjects = 0;

    // 2) Start the selection process
    m_selectionRecorder.enterSelectionMode(x, y, numObjects);

    // 3) Draw your objects, calling m_selectionRecorder.setObjectIndex() before drawing each one.

    // 4) Find out which object you selected, if any (-1 means no selection).
    m_selectionIndex = m_selectionRecorder.exitSelectionMode();
}
