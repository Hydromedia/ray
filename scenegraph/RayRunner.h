#ifndef RAYRUNNER_H
#define RAYRUNNER_H

#include <QRunnable>
#include <CS123Common.h>
#include "RayScene.h"
#include "Camera.h"


class RayRunner : public QRunnable
{
public:
    RayRunner(glm::vec2 start, glm::vec2 end, RayScene *scene);
    ~RayRunner();

    void run();

private:
    glm::vec2 m_start;
    glm::vec2 m_end;
    RayScene *m_scene;
};

#endif // RAYRUNNER_H
