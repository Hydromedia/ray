#include "RayScene.h"
#include "Settings.h"
#include "CS123SceneData.h"

#include "RayRunner.h"
#include <iostream>
#include "glm/ext.hpp"


RayScene::RayScene(const QList<SceneStruct> &graph,
                   const QList<CS123SceneLightData> &lights,
                   const CS123SceneGlobalData &glo,
                   Canvas2D *canvas) :
    Scene(graph, lights, glo),
    m_canvas(canvas)
{
    glm::vec4 m_backgroundColor = glm::vec4(0,0,0,0);
}

RayScene::~RayScene()
{

}

void RayScene::cancelRender()
{
    //m_continueRendering = false; test
}

void RayScene::startRender(Camera *camera, int numThreads)
{
    m_camera = camera;

    if (numThreads > 1){
    float index_iter = (((float)m_canvas->height())/((float)numThreads));
    for (int i = 0; i < numThreads; i++){
        RayRunner *runner = new RayRunner(glm::vec2(0, std::round(index_iter*i)), glm::vec2(0, std::round(index_iter*(i+1))), this);
        // QThreadPool takes ownership and deletes 'runner' automatically
        QThreadPool::globalInstance()->start(runner);
    }
    QThreadPool::globalInstance()->waitForDone();
    } else {
        renderGeometry(glm::vec2(0,0), glm::vec2(m_canvas->height(), m_canvas->width()));
    }
    m_canvas->repaint();
}

void RayScene::renderGeometry(glm::vec2 startIndex, glm::vec2 endIndex)
{
    int width = m_canvas->width();
    int height = m_canvas->height();
    BGRA *data = m_canvas->data();
    for (int y = startIndex.y; y < endIndex.y; y++) {
        for (int x = 0; x < m_canvas->width(); x++) {
            glm::vec4 lightingResult;
            if(settings.useSuperSampling) {
                float sampleSize = settings.numSuperSamples;
                float sampleIter = 1.f/sampleSize;
                bool continueSampling = true;
                int iterations = 4;

                //Test the corners to see if we want to do super sampling
                SuperSampleDecisionContainer corners = sampleCorners(x, y);
                lightingResult = corners.result;

                //Do super sampling
                if (corners.doSample == true) {
                    for (int i = 1; i < settings.numSuperSamples - 1 && continueSampling; i++) {
                        for (int j = 1; j < settings.numSuperSamples - 1 && continueSampling; j++) {
                            ObjectAndRayCollisionContainer objRay = CalculateIntersect(x, y, ((float)i) * sampleIter, ((float)j) * sampleIter);
                            if (objRay.container.intersection == true) {
                                CS123SceneColor ca = objRay.object.primitive.material.cAmbient;
                                CS123SceneColor cd = objRay.object.primitive.material.cDiffuse;
                                lightingResult +=
                                        calculateLighting(glm::vec4(ca.r, ca.g, ca.b, ca.a),
                                                          glm::vec4(cd.r, cd.g, cd.b, cd.a),
                                                         (objRay.model_space_ray.P + objRay.model_space_ray.d*objRay.container.t),
                                                          glm::normalize(convertObjectSpaceNormalToWorldSpaceNormal(glm::mat3(objRay.object.transformation), objRay.container.objectSpaceNormal)),
                                                          objRay);

                                iterations++;
                            }
                        }
                    }
                }
                lightingResult /= ((float)iterations);
                data[x + y*width] = BGRA(std::round(lightingResult.r*255.f),
                                         std::round(lightingResult.g*255.f),
                                         std::round(lightingResult.b*255.f),
                                         std::round(lightingResult.a*255.f));
            } else {
                ObjectAndRayCollisionContainer objRay = CalculateIntersect(x, y, .5f, .5f);
                if (objRay.container.intersection == true) {
                    CS123SceneColor ca = objRay.object.primitive.material.cAmbient;
                    CS123SceneColor cd = objRay.object.primitive.material.cDiffuse;
                    lightingResult =
                            calculateLighting(glm::vec4(ca.r, ca.g, ca.b, ca.a),
                                              glm::vec4(cd.r, cd.g, cd.b, cd.a),
                                             (objRay.model_space_ray.P + objRay.model_space_ray.d*objRay.container.t),
                                              glm::normalize(convertObjectSpaceNormalToWorldSpaceNormal(glm::mat3(objRay.object.transformation), objRay.container.objectSpaceNormal)),
                                              objRay);
                    data[x + y*width] = BGRA(std::round(lightingResult.r*255.f),
                                             std::round(lightingResult.g*255.f),
                                             std::round(lightingResult.b*255.f),
                                             std::round(lightingResult.a*255.f));
                }
            }
        }
    }
}

ObjectAndRayCollisionContainer RayScene::CalculateIntersect(float x, float y, float x_offset, float y_offset){
    // get sample in film plane;
    Ray model_space_ray = Ray();
    ObjectAndRayCollisionContainer ret;

    int width = m_canvas->width();
    int height = m_canvas->height();

    x += x_offset;
    y += y_offset;
    glm::vec4 Pfilm = glm::vec4(((2.f*((float)x))/((float)(width))) - 1, 1 - (2.f*((float)y))/((float)(height)), -1, 1);

    glm::mat4 film_to_world = glm::inverse(m_camera->getViewMatrix()) * glm::inverse(m_camera->getScaleMatrix());
    glm::vec4 Pworld = film_to_world * Pfilm;

    //determine closest object in scene hit by a ray going through that sample from eye point
    SceneStruct sceneStruct;
    RayCollisionContainer container;
    for (int i = 0; i < m_sceneGraph.length(); i++){

        //transform ray to object space
        glm::mat4 model_transform = m_sceneGraph.at(i).transformation;
        model_space_ray = Ray();

        model_space_ray.d = glm::normalize(Pworld - m_camera->getPosition());
        model_space_ray.P = m_camera->getPosition();
        Ray object_space_ray = convertRayToObjectSpace(model_space_ray, model_transform);

        RayCollisionContainer temp_container;

        if (m_sceneGraph.at(i).primitive.type == PRIMITIVE_CUBE) {
            temp_container = Ray::checkCube(object_space_ray);
        } else if (m_sceneGraph.at(i).primitive.type == PRIMITIVE_CONE) {
            temp_container = Ray::checkCone(object_space_ray);
        } else if (m_sceneGraph.at(i).primitive.type == PRIMITIVE_CYLINDER) {
            temp_container = Ray::checkCylinder(object_space_ray);
        } else if (m_sceneGraph.at(i).primitive.type == PRIMITIVE_SPHERE) {
            temp_container = Ray::checkSphere(object_space_ray);
        }

        if (temp_container.t < container.t && temp_container.t >= 0 && temp_container.intersection) {
            container = temp_container;
            sceneStruct = m_sceneGraph.at(i);
            ret.container = container;
            ret.object = sceneStruct;
            ret.model_space_ray = model_space_ray;
        }
    }

    return ret;
}

SuperSampleDecisionContainer RayScene::sampleCorners(float x, float y)
{
    SuperSampleDecisionContainer container;
    container.doSample = false;
    glm::vec4 lightingResult;
    glm::vec4 tempResult;
    QList<glm::vec4> results;
    for (int i = 0; i <= 1; i++) {
        for (int j = 0; j <= 1; j++) {
            ObjectAndRayCollisionContainer objRay = CalculateIntersect(x+i, j+y, 0, 0);
            if (objRay.container.intersection == true) {
                CS123SceneColor ca = objRay.object.primitive.material.cAmbient;
                CS123SceneColor cd = objRay.object.primitive.material.cDiffuse;
                tempResult =
                        calculateLighting(glm::vec4(ca.r, ca.g, ca.b, ca.a),
                                          glm::vec4(cd.r, cd.g, cd.b, cd.a),
                                         (objRay.model_space_ray.P + objRay.model_space_ray.d*objRay.container.t),
                                          glm::normalize(convertObjectSpaceNormalToWorldSpaceNormal(glm::mat3(objRay.object.transformation), objRay.container.objectSpaceNormal)),
                                          objRay);
                lightingResult+=tempResult;
                results.append(tempResult);
            } else {
                results.append(m_backgroundColor);
            }
        }
    }

    if (glm::length(results.at(0) - results.at(1)) > .15f ||
        glm::length(results.at(0) - results.at(2)) > .15f ||
        glm::length(results.at(0) - results.at(3)) > .15f ||
        glm::length(results.at(2) - results.at(1)) > .15f ||
        glm::length(results.at(2) - results.at(3)) > .15f ||
        glm::length(results.at(3) - results.at(1)) > .15f ){
        container.doSample = true;
    }
    container.result = lightingResult;
    return container;
}

glm::vec4 RayScene::calculateLighting(glm::vec4 objectAmbient, glm::vec4 objectDiffuse, glm::vec4 collisionPoint, glm::vec4 N,  ObjectAndRayCollisionContainer object)
{
    QImage image;
    CS123SceneFileMap *textureMap = object.object.primitive.material.textureMap;
    if (textureMap->isUsed == true) {
        QString s = QString(QString::fromStdString(textureMap->filename));
        image = QImage(s);
    }
    glm::vec4 resultColor = objectAmbient;
    for (int i = 0; i < this->m_sceneLights.length(); i++) {
        CS123SceneLightData m = m_sceneLights.at(i);
        if (m.type == LIGHT_POINT) {
            glm::vec3 norm_temp = glm::normalize(glm::vec3(m.pos - collisionPoint));
            glm::vec4 LightNormal = glm::vec4(norm_temp.x, norm_temp.y, norm_temp.z, 0);
            //N = glm::normalize(N);
            //Is this commutative? Because it would make more sense to me to put the multiplication outside of the dot product for nicety's sake -HOURS-
            glm::vec4 Imgamma = glm::vec4(m.color.r, m.color.g, m.color.b, m.color.a);
            glm::vec4 objectColor = objectDiffuse;
            if (textureMap->isUsed == true) {
                int s = (int(object.container.textureCoord.x*image.width()*textureMap->repeatU)) % image.width();
                int t = (int(object.container.textureCoord.y*image.height()*textureMap->repeatV)) % image.height();
                QColor color = image.pixel(s, t);
                objectColor = glm::mix(objectDiffuse, glm::vec4(color.red(), color.green(), color.blue(), 1), object.object.primitive.material.blend);
            }
            resultColor += Imgamma * (objectColor * glm::clamp((glm::dot(glm::vec3(N), glm::vec3(LightNormal))), 0.f, 1.f));

        }
    }
    return glm::vec4(resultColor.x, resultColor.y, resultColor.z, 1);
}

glm::vec4 RayScene::convertObjectSpaceNormalToWorldSpaceNormal(glm::mat3 M3, glm::vec4 objectSpaceNormal)
{
    //Is this right? -HOURS-
    glm::vec3 normal = glm::vec3(objectSpaceNormal.x, objectSpaceNormal.y, objectSpaceNormal.z);
    M3 = glm::transpose(M3);
    glm::mat3 M3ti = glm::inverse(M3);

    glm::vec3 temp = (M3ti * normal);
    return glm::vec4(temp.x, temp.y, temp.z, 0);
}

Ray RayScene::convertRayToObjectSpace(Ray r, glm::mat4 M)
{
    r.d = glm::inverse(M) * r.d;
    r.P = glm::inverse(M) * r.P;
    return r;
}
