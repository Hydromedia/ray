#ifndef INVERTFILTER_H
#define INVERTFILTER_H

#include "filter/filter.h"

class InvertFilter : public Filter
{
public:
    InvertFilter();
    ~InvertFilter();

    void applyFilter(Canvas2D *canvas, Settings s);
};

#endif // INVERTFILTER_H
