#include "edgedetectfilter.h"
#include "Canvas2D.h"

EdgeDetectFilter::EdgeDetectFilter() : Filter()
{

}

EdgeDetectFilter::~EdgeDetectFilter()
{

}

void EdgeDetectFilter::convolve(Canvas2D *canvas)
{
    setStartAndStop(canvas);

    int size = ((m_stop.y() - m_start.y()) * (m_stop.x() - m_start.x()));
    std::cout << size << std::endl;
    glm::vec4 *start_buffer = new glm::vec4[size];

    int convolutionBufferIndex = 0;
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            int ind = i*canvas->width() + j;
             start_buffer[convolutionBufferIndex] = glm::vec4(((float)canvas->data()[ind].r)/255.f,
                                                           ((float)canvas->data()[ind].g)/255.f,
                                                           ((float)canvas->data()[ind].b)/255.f,
                                                           ((float)canvas->data()[ind].a)/255.f);
             convolutionBufferIndex++;

        }
    }

    float filter1 [] = {1, 2, 1};

    float filter2 [] = {1, 0, -1};

    float filter3 [] = {-1, 0, 1};

    m_convolutionBuffer_1 = new glm::vec4[size];
    m_a_buffer = new glm::vec4[size];

    m_convolutionBuffer_2 = new glm::vec4[size];
    m_b_buffer = new glm::vec4[size];

    //Filling A buffer with Gx
    convolutionBufferIndex = 0;
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            m_convolutionBuffer_1[convolutionBufferIndex] = convolveHelp(start_buffer, canvas, filter1, 3, 1, glm::vec2(j - m_start.x(), i-m_start.y()), m_start, m_stop, false);
            convolutionBufferIndex++;
        }
    }

    convolutionBufferIndex = 0;
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            m_a_buffer[convolutionBufferIndex] = convolveHelp(m_convolutionBuffer_1, canvas, filter3, 1, 3, glm::vec2(j - m_start.x(), i-m_start.y()), m_start, m_stop, false);
            convolutionBufferIndex++;
        }
    }

    //Filling B buffer with Gy
    convolutionBufferIndex = 0;
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            m_convolutionBuffer_2[convolutionBufferIndex] = convolveHelp(start_buffer, canvas, filter2, 3, 1, glm::vec2(j - m_start.x(), i-m_start.y()), m_start, m_stop, false);
            convolutionBufferIndex++;
        }
    }

    convolutionBufferIndex = 0;
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            m_b_buffer[convolutionBufferIndex] = convolveHelp(m_convolutionBuffer_2, canvas, filter1, 1, 3, glm::vec2(j - m_start.x(), i-m_start.y()), m_start, m_stop, false);
            convolutionBufferIndex++;
        }
    }

    delete [] start_buffer;
    delete [] m_convolutionBuffer_1;
    delete [] m_convolutionBuffer_2;
}

void EdgeDetectFilter::applyFilter(Canvas2D *canvas, Settings s)
{

    GreyscaleFilter f = GreyscaleFilter();
    f.applyFilter(canvas, s);

    convolve(canvas);

    BGRA* pix = canvas->data();
    int convolutionBufferIndex = 0;
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            int pixel_index = i*(canvas->width()) + j;
            pix[pixel_index].r = (char) (cap((s.edgeDetectThreshold)*std::sqrt(m_a_buffer[convolutionBufferIndex].x*m_a_buffer[convolutionBufferIndex].x +
                                                                               m_b_buffer[convolutionBufferIndex].x*m_b_buffer[convolutionBufferIndex].x))*255.f);

            pix[pixel_index].g = (char) (cap((s.edgeDetectThreshold)*std::sqrt(m_a_buffer[convolutionBufferIndex].y*m_a_buffer[convolutionBufferIndex].y +
                                                                               m_b_buffer[convolutionBufferIndex].y*m_b_buffer[convolutionBufferIndex].y))*255.f);

            pix[pixel_index].b = (char) (cap((s.edgeDetectThreshold)*std::sqrt(m_a_buffer[convolutionBufferIndex].z*m_a_buffer[convolutionBufferIndex].z +
                                                                               m_b_buffer[convolutionBufferIndex].z*m_b_buffer[convolutionBufferIndex].z))*255.f);
            convolutionBufferIndex++;
        }
    }

    delete [] m_a_buffer;
    delete [] m_b_buffer;
}

float EdgeDetectFilter::cap (float f) {
    if (f < 0) {
        f = 0;
    } else if (f > 1) {
        f = 1;
    }

    return f;
}
