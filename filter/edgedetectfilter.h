#ifndef EDGEDETECTFILTER_H
#define EDGEDETECTFILTER_H

#include "filter.h"

class EdgeDetectFilter : public Filter
{
public:
    EdgeDetectFilter();
    ~EdgeDetectFilter();

private:
    void convolve(Canvas2D *canvas);
    void applyFilter(Canvas2D *canvas, Settings s);
    float cap (float f);

    glm::vec4 * m_a_buffer;
    glm::vec4 * m_b_buffer;
};

#endif // EDGEDETECTFILTER_H
