#include "scalefilter.h"
#include "Canvas2D.h"

ScaleFilter::ScaleFilter() : Filter()
{

}

ScaleFilter::~ScaleFilter()
{

}

double ScaleFilter::g(double x, double a){
    double radius;
    if (a < 1) {
        radius = 1.0/a;
    } else {
        radius = 1.0;
    }

    double fabs_x = fabs(x);
    if (fabs_x > radius) {
        return 0;
    } else {
        return (1 - fabs_x/radius) / radius;
    }
}

void ScaleFilter::convolve(Canvas2D *canvas, Settings s)
{
    setStartAndStop(canvas);

    int size = ((m_stop.y() - m_start.y()) * (m_stop.x() - m_start.x()));
    std::cout << size << std::endl;
    glm::vec4 *start_buffer = new glm::vec4[size];

    int convolutionBufferIndex = 0;
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            int ind = i*canvas->width() + j;
             start_buffer[convolutionBufferIndex] = glm::vec4(((float)canvas->data()[ind].r)/255.f,
                                                           ((float)canvas->data()[ind].g)/255.f,
                                                           ((float)canvas->data()[ind].b)/255.f,
                                                           ((float)canvas->data()[ind].a)/255.f);
             convolutionBufferIndex++;

        }
    }

    QPoint destinationSize1 = QPoint((m_stop.x() - m_start.x())*s.scaleX, (m_stop.y() - m_start.y()));
    m_convolutionBuffer_1 = new glm::vec4[destinationSize1.x()*destinationSize1.y()];
    convolutionBufferIndex = 0;
    for (int i = 0; i < destinationSize1.y(); i++) {
        for (int j = 0; j < destinationSize1.x(); j++) {
            float a = s.scaleX;
            m_convolutionBuffer_1[convolutionBufferIndex] = hprimeX(start_buffer, a, j, m_start, m_stop, i);
            convolutionBufferIndex++;
        }
    }

    QPoint destinationSize2 = QPoint((m_stop.x() - m_start.x())*s.scaleX, (m_stop.y() - m_start.y())*s.scaleY);
    m_convolutionBuffer_2 = new glm::vec4[destinationSize2.x()*destinationSize2.y()];
    convolutionBufferIndex = 0;
    for (int i = 0; i < destinationSize2.y(); i++) {
        for (int j = 0; j < destinationSize2.x(); j++) {
            float a = s.scaleY;
            m_convolutionBuffer_2[convolutionBufferIndex] = hprimeY(m_convolutionBuffer_1, a, i, QPoint(0,0), destinationSize1, j);
            convolutionBufferIndex++;
        }
    }

    m_start = QPoint(0,0);
    m_stop = destinationSize2;

    delete [] m_convolutionBuffer_1;
    delete [] start_buffer;
}

glm::vec4 ScaleFilter::hprimeX(glm::vec4 *array, float a, int k, QPoint start, QPoint stop, int y)
{
    float source_center = k/a + (1-a)/(2*a);
    float support = (a > 1) ? 1 : 1/a; //ternary operator

    int left = ceil(source_center - support);

    int right = floor(source_center + support);

    glm::vec4 sum = glm::vec4(0,0,0,0);
    float sum_weights = 0;
    for (int j = left; j <= right; j++) {

        int use_j = j;

        if (0 >= j) {
            use_j = 0;
        } else if (stop.x()-start.x()-1 < j) {
            use_j = (stop.x()-start.x()-1);
        }

        int array_index = y*(stop.x() - start.x()) + use_j;
        double val = g(j - source_center, a);
        sum = sum + glm::vec4(array[array_index].x*val,
                              array[array_index].y*val,
                              array[array_index].z*val,
                              0);
        sum_weights += val;
    }
    return sum/sum_weights;
}

glm::vec4 ScaleFilter::hprimeY(glm::vec4 *array, float a, int k, QPoint start, QPoint stop, int x)
{
    float source_center = k/a + (1-a)/(2*a);
    float support = (a > 1) ? 1 : 1/a; //ternary operator

    int left = ceil(source_center - support);

    int right = floor(source_center + support);

    glm::vec4 sum = glm::vec4(0,0,0,0);
    float sum_weights = 0;
    for (int j = left; j <= right; j++) {

        int use_j = j;

        if (0 >= j) {
            use_j = 0;
        } else if (stop.y()-start.y()-1 < j) {
            use_j = (stop.y()-start.y()-1);
        }

        int array_index = use_j*(stop.x() - start.x()) + x;
        double val = g(j - source_center, a);
        sum = sum + glm::vec4(array[array_index].x*val,
                              array[array_index].y*val,
                              array[array_index].z*val,
                              0);
        sum_weights += val;
    }
    return sum/sum_weights;
}

void ScaleFilter::applyFilter(Canvas2D *canvas, Settings s)
{

    convolve(canvas, s);
    canvas->resize(m_stop.x() - m_start.x()+1, (m_stop.y() - m_start.y())+1);
    BGRA* pix = canvas->data();
    int convolutionBufferIndex = 0;
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            int pixel_index = i*(canvas->width()) + j;
            pix[pixel_index].r = (m_convolutionBuffer_2[convolutionBufferIndex].x*255.f);
            pix[pixel_index].g = (m_convolutionBuffer_2[convolutionBufferIndex].y*255.f);
            pix[pixel_index].b = (m_convolutionBuffer_2[convolutionBufferIndex].z*255.f);
            convolutionBufferIndex++;
        }
    }
    delete [] m_convolutionBuffer_2;
}
