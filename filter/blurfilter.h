#ifndef BLURFILTER_H
#define BLURFILTER_H

#include "filter.h"

class BlurFilter : public Filter
{
public:
    BlurFilter();
    ~BlurFilter();

    void convolve(Canvas2D *canvas, Settings s);
    void applyFilter(Canvas2D *canvas, Settings s);
};

#endif // BLURFILTER_H
