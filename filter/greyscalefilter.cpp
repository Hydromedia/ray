#include "greyscalefilter.h"
#include "Canvas2D.h"

GreyscaleFilter::GreyscaleFilter()
{

}

GreyscaleFilter::~GreyscaleFilter()
{

}

void GreyscaleFilter::applyFilter(Canvas2D *canvas, Settings s)
{
    setStartAndStop(canvas);

    BGRA* pix = canvas->data();
    for (int i = m_start.y(); i < m_stop.y(); i++) {
        for (int j = m_start.x(); j < m_stop.x(); j++) {
            int pixel_index = i*(canvas->width()) + j;

            int intensity = (int)
                            ((.299f * ((float)pix[pixel_index].r)) +
                             (.587f * ((float)pix[pixel_index].g)) +
                             (.114 * ((float)pix[pixel_index].b)));

            pix[pixel_index].r = intensity;
            pix[pixel_index].g = intensity;
            pix[pixel_index].b = intensity;
        }
    }
}

