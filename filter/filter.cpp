#include "filter.h"
#include "Canvas2D.h"
#include "Settings.h"

Filter::Filter()
{

}

Filter::~Filter()
{

}


glm::vec4 Filter::convolveHelp(glm::vec4 *array, Canvas2D *canvas, float *filter, int filterHeight, int filterWidth, glm::vec2 center, QPoint start, QPoint stop, bool averaged)
{
    //filterWidth++;
    //filterHeight++;

    int start_of_array_X = (((int)(center.x)) - (filterWidth)/2);
    int start_of_array_Y = (((int)(center.y)) - (filterHeight)/2);

    int end_of_array_X = (((int)(center.x)) + (filterWidth)/2);
    int end_of_array_Y = (((int)(center.y)) + (filterHeight)/2);

    glm::vec4 sum = glm::vec4(0,0,0,0);
    float sum_weights = 0;
    for (int i = start_of_array_Y; i <= end_of_array_Y; i++) {
        for (int j = start_of_array_X; j <= end_of_array_X; j++) {
            int f_i = i - start_of_array_Y;
            int f_j = j - start_of_array_X;

            int use_i = i;
            int use_j = j;

            int filter_index = f_i*(filterWidth) + f_j;

            if (0 >= j) {
                use_j = 0;
            } else if (stop.x()-start.x()-1 < j) {
                use_j = (stop.x()-start.x()-1);
            } if (0 > i) {
                use_i = 0;
            } else if (stop.y()-start.y()-1 < i) {
                use_i = (stop.y()-start.y()-1);
            }

            int array_index = use_i*(stop.x() - start.x()) + use_j;
            double val = filter[filter_index];
            sum = sum + glm::vec4(array[array_index].x*val,
                                  array[array_index].y*val,
                                  array[array_index].z*val,
                                  0);
            sum_weights +=val;
        }
    }

    if (averaged){
        return sum/sum_weights;
    } else {
        return sum;
    }
}

void Filter::setStartAndStop(Canvas2D *canvas)
{
    QPoint start = canvas->marqueeStart();
    QPoint stop = canvas->marqueeStop();
    if (start.y() == stop.y() && start.x() == stop.x()) {
        m_start = QPoint(0,0);
        m_stop = QPoint(canvas->width()-1,canvas->height()-1);
    } else {
        m_start = QPoint(min(start.x(), stop.x()), min(start.y(), stop.y()));
        m_stop = QPoint(max(start.x(), stop.x()), max(start.y(), stop.y()));
    }
}

void Filter::fillGaussian(int size)
{
    m_kernel = new float [size*size];

    float o = (size/2)/3.f;

    float sum = 0;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            float y = (2.f*((float)i)/size)-1;
            float x = (2.f*((float)j)/size)-1;
            float prod = exp(-(x*x + y*y)/2.f*o*o);
            m_kernel[i*size+j] = (1.f/(2.f*M_PI*o*o))*prod;

            sum += m_kernel[i*size+j];
        }
    }


    // Normalizing
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            m_kernel[i*size+j] /= sum;
            //std::cout << i << ", " << j << ": " << m_kernel[i*size+j] << std::endl;
        }
    }
}
