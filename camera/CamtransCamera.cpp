/**
 * @file   CamtransCamera.cpp
 *
 * This is the perspective camera class you will need to fill in for the Camtrans lab.  See the
 * lab handout for more details.
 */

#include "CamtransCamera.h"
#include <Settings.h>

CamtransCamera::CamtransCamera() :
    m_aspect(1), m_heightAngle(glm::radians(60.f)), m_near(1.f), m_far(30.f), m_eye(glm::vec4(2, 2, 2, 1)), m_view(glm::mat4x4()), m_projection(glm::mat4x4()),
    m_scale(glm::mat4x4()), m_perspective(glm::mat4x4()), m_u(glm::vec4()), m_w(glm::vec4()), m_v(glm::vec4()),
    m_look(glm::vec4(-2, -2, -2, 0)), m_up(glm::vec4(0, 1, 0, 0))
{
    //updateMatrices();
    orientLook(m_eye, m_look, m_up);
    //this shit? I mean the only thing that should happen is that the constructor would be called and then orient look
}

void CamtransCamera::updateMatrices()
{
    m_w = glm::normalize(-getLook());
    //normalizing correctly?
    m_v = glm::normalize(getUp() - (glm::dot(getUp(), m_w)* m_w));
    m_u = glm::vec4(glm::cross(glm::vec3(m_v), glm::vec3(m_w)), 0);

    float c = -m_near/m_far;
    m_perspective = glm::transpose(glm::mat4(1, 0, 0, 0,
                                             0, 1, 0, 0,
                                             0, 0, -1.f/(1+c), c/(1.f+c),
                                             0, 0, -1.f, 0));

    //float width = m_aspect*m_far*glm::tan(m_heightAngle/2.f);
    // this should be w/2, which is equal to far*tan(thetaw/2) : m_aspect*m_far*glm::tan(m_heightAngle/2.f))

    m_scale = glm::transpose(glm::mat4(1/(m_aspect*m_far*glm::tan(m_heightAngle/2.f)), 0, 0, 0,
                                       0, 1/(m_far*glm::tan(m_heightAngle/2.f)), 0, 0,
                                       0, 0, 1/m_far, 0,
                                       0, 0, 0, 1));

    glm::mat4 M3 = glm::transpose(glm::mat4(m_u.x, m_u.y, m_u.z, 0,
                                            m_v.x, m_v.y, m_v.z, 0,
                                            m_w.x, m_w.y, m_w.z, 0,
                                            0, 0, 0, 1));

    glm::mat4 M4 = glm::transpose(glm::mat4(1, 0, 0, -m_eye.x,
                                            0, 1, 0, -m_eye.y,
                                            0, 0, 1, -m_eye.z,
                                            0, 0, 0, 1));

    m_view = M3 * M4;

    m_projection = m_perspective*m_scale;

//    std::cout << "Proj: " << std::endl;
//    std::cout << glm::to_string(m_projection) << std::endl;
//    std::cout << "View: "<< std::endl;
//    std::cout << glm::to_string(m_view) << std::endl;
}

void CamtransCamera::orientLook(const glm::vec4 &eye, const glm::vec4 &look, const glm::vec4 &up)
{
    m_eye = eye;
    m_look = glm::normalize(look);
    m_up = glm::normalize(up);
    updateMatrices();

}

void CamtransCamera::translate(const glm::vec4 &v)
{
    m_eye += v;
    std::cout << "TRANSLATE!!"<<std::endl;
    std::cout << "eye: " << glm::to_string(m_eye) << std::endl;
}

void CamtransCamera::updateLookAndUp(){
    m_look = -m_w;
    m_up = glm::normalize(m_v -(glm::dot(m_v, m_w) * m_w));
    std::cout << "ROTATE!"<<std::endl;
}

void CamtransCamera::rotateU(float degrees)
{
    //pitch
    float angle = glm::radians(degrees);
    m_v = m_v * glm::cos(angle) - m_w * glm::sin(angle);
    m_w = m_v * glm::sin(angle) + m_w * glm::cos(angle);
    updateLookAndUp();
    orientLook(m_eye, m_look, m_up);

}

void CamtransCamera::rotateV(float degrees)
{
    //yaw
    float angle = glm::radians(degrees);
    m_u = m_u * glm::cos(angle) - m_w * glm::sin(angle);
    m_w = m_u * glm::sin(angle) + m_w * glm::cos(angle);
    updateLookAndUp();
    orientLook(m_eye, m_look, m_up);

}

void CamtransCamera::rotateW(float degrees)
{
    //roll/spin
    float angle = glm::radians(degrees);
    m_u = m_v * glm::sin(angle) + m_u * glm::cos(angle);
    m_v = m_v * glm::cos(angle) - m_u * glm::sin(angle);
    updateLookAndUp();
    orientLook(m_eye, m_look, m_up);

}

glm::mat4x4 CamtransCamera::getProjectionMatrix() const
{
    return m_projection;
}

glm::mat4x4 CamtransCamera::getViewMatrix() const
{
    return m_view;
}

glm::mat4x4 CamtransCamera::getScaleMatrix() const
{
    return m_scale;
}

glm::mat4x4 CamtransCamera::getPerspectiveMatrix() const
{
    return m_perspective;
}

glm::vec4 CamtransCamera::getPosition() const
{
    return m_eye;
}

glm::vec4 CamtransCamera::getLook() const
{
    return m_look;
}

glm::vec4 CamtransCamera::getUp() const
{
    return m_up;
}

float CamtransCamera::getAspectRatio() const
{
    return m_aspect;
}

float CamtransCamera::getHeightAngle() const
{
    return m_heightAngle;
}

void CamtransCamera::setHeightAngle(float h)
{
    m_heightAngle = glm::radians(h);
    orientLook(m_eye, m_look, m_up);

}

void CamtransCamera::setAspectRatio(float a)
{
    m_aspect = a;
    orientLook(m_eye, m_look, m_up);
}

void CamtransCamera::setClip(float nearPlane, float farPlane)
{
    m_near = nearPlane;
    m_far = farPlane;
    orientLook(m_eye, m_look, m_up);

}
