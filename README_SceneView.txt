I used the given structs to make my own struct called SceneStrcut which is defined in Scene.h. I use this for my scenegraph, which is a QList of SceneStructs. I fill the scenegraph in the given methods, with
the only change being that I changed the method signature of addPrimitive so that I could use a copy and multiply by Ka and Kd, as using a reference and saving that causes the scene to progressively darken.

I also implemented variable level of detail, which is handled by a method in scene called applyLOD(float numShapes). This takes in the number of shapes as a float (for math convenience's sake) and changes 
initializes the buffers of the shapes in Scene (m_cube etc) with a logarithmically scaling level of detail. The equation makes it so that a scene with a lot of shapes will have a low LOD which gets lower
slower based on the increase in number of shapes. 

No known bugs/crashes.