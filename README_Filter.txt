Design Decisions:
Initially I thought that I would be able to have one function performing the inner loop of my convolution. I realized, however, that this was not possible, and after spending a long time
writing what i thought that would be (Filter::convolveHelp) I gave up on trying to fully abstract all of my convolution in the interest of having a working project. Consequently, I ran out
of time and have a much more poorly designed and abstracted project than I would have liked. I have a filter class from which all my other filters inherit. The class contains some common functionality
as well as buffers that a majority of the filters use. Otherwise it's purely convenient to have a set of filters to call applyFilter on.

Issues:
My blur doesn't work all the time. Not really sure why. It does work a lot of the time though, so if it doesn't work the first time for you id ask you to keep trying it until it does. It uses 2d gaussian
convolution.

Required "Extra": Median

