I chose to Have a base Shape class from which my various shapes inherited. This shape class contains a bunch of common functionality that all the shapes need to do, essentially everything except how to update the individual shapes' buffer.

The shapescene class I just used to detect settings changes and respond to them. It merely has a pointer to the current shape and changes that when there is a settings change, and uses the pointer to draw the shape.

No known bugs/crashes.